#!/usr/bin/env python3

import codecs, datetime, hashlib, re, sys, socket # do not use any other imports/libraries
from urllib.parse import urlparse
from pyasn1.codec.der import decoder, encoder
from pyasn1.type import namedtype, univ

# sudo apt install python3-pyasn1-modules
from pyasn1_modules import rfc2560, rfc5280

# took x.y hours (please specify here how much time your solution required)

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i

#==== ASN1 encoder start ====
def bstrn(bstr):
    i = 0

    for bit in bstr:
        i<<=1
        if(bit =='1'):
            i|=1

    return i


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    ret = 0

    if(len(value_bytes) >= 128):
        ret = bytes([128 | len(nb(len(value_bytes)))]) + nb(len(value_bytes))
    else:
        ret = bytes([len(value_bytes)])

        if(ret == 0):
            ret = 1
    
    return ret


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'

    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_null():
    return bytes([0x05]) + bytes([0x00])
    


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER

    convertedI = nb(i)
    #print(i,convertedI)

    if(convertedI == b''):
        b = bytes([0x00])
    else:
        if(convertedI[0]>= 128):
            b = bytes([0x00]) + nb(i)
        else:
            b = nb(i)

    return bytes([0x02]) + asn1_len(b) + b

def asn1_bitstring(bitstr):
    if(bitstr == ''):
        return bytes([0x03]) + bytes([0x01]) + bytes([0x0])

    padding = 8 - len(bitstr) % 8
    if(padding == 8):
        padding = 0

    bitstr = bitstr + ('0' * padding)
    i = 0

    bitstrSplitted = [bitstr[i:i+8] for i in range(0, len(bitstr), 8)]
    data = []
    for chunk in bitstrSplitted:
        data.append(bstrn(chunk))

    if(padding == 0):
        padding = bytes([0x00])
    else:
        padding = nb(padding)

    res = padding + bytes(data)

    return bytes([0x03]) + asn1_len(res) + res

def asn1_bitstring_der(bitstr_der):

    res = bytes([0x00]) + bitstr_der

    return bytes([0x03]) + asn1_len(res) + res

def asn1_utf8(utf8_str):
    res = bytes(utf8_str, 'UTF-8')

    return bytes([0x0c]) + asn1_len(res) + res



def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    counter = 0
    firstOctet = 40
    biteDataSet = []
    for item in oid:
        if(counter < 1):
            firstOctet = firstOctet * item
            
        elif(counter < 2):
            firstOctet = firstOctet + item
            biteDataSet.append(firstOctet)
        else:
            b = ''
            tempBiteDataSet = []
            prependForFirst = '0' if(item < 255) else '1' 
            lastBinaryUsed = 0
            while item:
                append = '1' if (item % 2) else '0'
                b = b + append

                if(len(b) == 7):
                    if(lastBinaryUsed == 0):
                        prepend = '0'
                        prependForFirst = '1'
                        lastBinaryUsed = 1
                    else:
                        prepend = '1' if(item) else '0' 

                    tempBiteDataSet.insert(0, bstrn(prepend + b[::-1]))

                    b = ''

                item >>= 1
                

            if(b != ''):
                tempBiteDataSet.insert(0, bstrn(prependForFirst + '0' * (7 - len(b[::1])) + b[::-1]))

            biteDataSet = biteDataSet + tempBiteDataSet

        counter = counter + 1


    oid = bytes(biteDataSet)

    return bytes([0x06]) + asn1_len(oid) + oid


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    #constructed
    return bytes([bstrn('110000')]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    #constructed
    return bytes([bstrn('110001')]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    #constructed
    return bytes([bstrn('10011')]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    #constructed
    return bytes([bstrn('10111')]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 + tag]) + asn1_len(der) + der


#==== ASN1 encoder end ====


def pem_to_der(content):
    # converts PEM-encoded X.509 certificate (if it is in PEM) to DER
    if content[:2] == b'--':
        content = content.replace(b"-----BEGIN CERTIFICATE-----", b"")
        content = content.replace(b"-----END CERTIFICATE-----", b"")
        content = codecs.decode(content, 'base64')
    return content

def get_name(cert):
    # gets subject DN from certificate
    return encoder.encode(decoder.decode(cert)[0][0][5])

def get_key(cert):
     # gets subjectPublicKey from certificate
    subjectPKInfo = decoder.decode(cert)[0][0][6][1]

    return subjectPKInfo

def get_serial(cert):
    # gets serial from certificate
    return int(decoder.decode(cert)[0][0][1])
def bitstring_to_bytes(s):
    v = int(s, 2)
    b = bytearray()
    while v:
        b.append(v & 0xff)
        v >>= 8
    return bytes(b[::-1])

def produce_request(cert, issuer_cert):
    # makes OCSP request in ASN.1 DER form

    # construct CertID (use SHA1)
    issuer_name = get_name(issuer_cert)
    issuer_key = get_key(issuer_cert)
    serial = get_serial(cert)

    #print(issuer_key)
    issuer_key_bytes = bytes(int(str(issuer_key[i : i + 8]), 2) for i in range(0, len(issuer_key), 8))

    print("[+] OCSP request for serial:", serial)

    # construct entire OCSP request
    request = asn1_sequence(asn1_sequence(asn1_sequence(asn1_sequence(asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(hashlib.sha1(issuer_name).digest()) + asn1_octetstring(hashlib.sha1(issuer_key_bytes).digest()) + asn1_integer(serial))))))

    return request

def send_req(ocsp_req, ocsp_url):
    # sends OCSP request to OCSP responder

    # parse OCSP responder's url
    host = urlparse(ocsp_url).netloc

    print("[+] Connecting to %s..." % (host))
    # connect to host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, 80))

    # send HTTP POST request
    s.send(b'POST / HTTP/1.1\r\nHost: ' + bytes(host, 'utf-8') + b'\r\nContent-Length: ' + bytes(str(len(ocsp_req)), 'utf-8') + b'\r\nContent-type: application/ocsp-request\r\nConnection: close\r\n\r\n' + ocsp_req)

    # read HTTP response header
    header = b''
    while(True):
        header = header + s.recv(1)
        if(header[-4:] == b'\r\n\r\n'):
            break

    # get HTTP response length
    length = re.search('content-length:\s*(\d+)\s', header.decode(), re.S+re.I).group(1)

    # read HTTP response body
    ocsp_resp = s.recv(int(length))

    return ocsp_resp

def get_ocsp_url(cert):
    # gets the OCSP responder's url from the certificate's AIA extension


    # pyasn1 syntax description to decode AIA extension
    class AccessDescription(univ.Sequence):
      componentType = namedtype.NamedTypes(
        namedtype.NamedType('accessMethod', univ.ObjectIdentifier()),
        namedtype.NamedType('accessLocation', rfc5280.GeneralName()))

    class AuthorityInfoAccessSyntax(univ.SequenceOf):
      componentType = AccessDescription()

    # looping over certificate extensions
    for seq in decoder.decode(cert)[0][0][7]:
        if str(seq[0])=='1.3.6.1.5.5.7.1.1': # look for AIA extension
            ext_value = bytes(seq[1])
            for aia in decoder.decode(ext_value, asn1Spec=AuthorityInfoAccessSyntax())[0]:
                if str(aia[0])=='1.3.6.1.5.5.7.48.1': # ocsp url
                    return str(aia[1].getComponentByName('uniformResourceIdentifier'))

    print("[-] OCSP url not found in the certificate!")
    exit(1)

def get_issuer_cert_url(cert):
    # gets the CA's certificate URL from the certificate's AIA extension (hint: see get_ocsp_url())
    # pyasn1 syntax description to decode AIA extension
    class AccessDescription(univ.Sequence):
      componentType = namedtype.NamedTypes(
        namedtype.NamedType('accessMethod', univ.ObjectIdentifier()),
        namedtype.NamedType('accessLocation', rfc5280.GeneralName()))

    class AuthorityInfoAccessSyntax(univ.SequenceOf):
      componentType = AccessDescription()

    # looping over certificate extensions
    for seq in decoder.decode(cert)[0][0][7]:

        if str(seq[0])=='1.3.6.1.5.5.7.1.1': # look for AIA extension
            ext_value = bytes(seq[1])
            for aia in decoder.decode(ext_value, asn1Spec=AuthorityInfoAccessSyntax())[0:1]:
                if str(aia[1][0])=='1.3.6.1.5.5.7.48.2': # issuer url
                    return str(aia[1][1].getComponentByName('uniformResourceIdentifier'))

    print("[-] OCSP issuer url not found in the certificate!")
    exit(1)

def download_issuer_cert(issuer_cert_url):
    # downloads issuer certificate
    print("[+] Downloading issuer certificate from:", issuer_cert_url)

    # parse issuer certificate url
    url = urlparse(issuer_cert_url)
    # connect to host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((url.netloc, 80))

    # send HTTP GET request
    s.send(b'GET ' + bytes(url.path, 'utf-8') + b' HTTP/1.1\r\nHost: ' + bytes(url.netloc, 'utf-8') + b'\r\n\r\n')
    
    # read HTTP response header
    header = b''
    while(True):
        header = header + s.recv(1)
        if(header[-4:] == b'\r\n\r\n'):
            break

    # get HTTP response length
    length = re.search('content-length:\s*(\d+)\s', header.decode(), re.S+re.I).group(1)
    
    # read HTTP response body
    issuer_cert = s.recv(int(length))

    return issuer_cert

def parse_ocsp_resp(ocsp_resp):
    # parses OCSP response
    ocspResponse, _ = decoder.decode(ocsp_resp, asn1Spec=rfc2560.OCSPResponse())
    responseStatus = ocspResponse.getComponentByName('responseStatus')
    assert responseStatus == rfc2560.OCSPResponseStatus('successful'), responseStatus.prettyPrint()
    responseBytes = ocspResponse.getComponentByName('responseBytes')
    responseType = responseBytes.getComponentByName('responseType')
    assert responseType == rfc2560.id_pkix_ocsp_basic, responseType.prettyPrint()

    response = responseBytes.getComponentByName('response')

    basicOCSPResponse, _ = decoder.decode(
        response, asn1Spec=rfc2560.BasicOCSPResponse()
    )

    tbsResponseData = basicOCSPResponse.getComponentByName('tbsResponseData')

    response0 = tbsResponseData.getComponentByName('responses').getComponentByPosition(0)

    producedAt = datetime.datetime.strptime(str(tbsResponseData.getComponentByName('producedAt')), '%Y%m%d%H%M%SZ')
    certID = response0.getComponentByName('certID')
    certStatus = response0.getComponentByName('certStatus').getName()
    thisUpdate = datetime.datetime.strptime(str(response0.getComponentByName('thisUpdate')), '%Y%m%d%H%M%SZ')
    nextUpdate = datetime.datetime.strptime(str(response0.getComponentByName('nextUpdate')), '%Y%m%d%H%M%SZ')

    # let's assume that the certID in the response matches the certID sent in the request

    # let's assume that the response is signed by a trusted responder

    print("[+] OCSP producedAt:", producedAt)
    print("[+] OCSP thisUpdate:", thisUpdate)
    print("[+] OCSP nextUpdate:", nextUpdate)
    print("[+] OCSP status:", certStatus)

cert = pem_to_der(open(sys.argv[1], 'rb').read())

ocsp_url = get_ocsp_url(cert)
print("[+] URL of OCSP responder:", ocsp_url)

issuer_cert_url = get_issuer_cert_url(cert)
issuer_cert = download_issuer_cert(issuer_cert_url)

ocsp_req = produce_request(cert, issuer_cert)
ocsp_resp = send_req(ocsp_req, ocsp_url)
parse_ocsp_resp(ocsp_resp)
