#!/usr/bin/env python3
import os, sys       # do not use any other imports/libraries
# took 8.0 hours


def nb(i, length):
    # converts integer to bytes
    b = b''
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i


def encrypt(pfile, kfile, cfile):
    pfile = open(pfile, 'rb').read()
    urandom = os.urandom(len(pfile))


    kfile = open(kfile, 'wb')
    kfile.write(urandom)
    kfile.close()

    cfile = open(cfile, 'wb')
    cfile.write(nb(bn(pfile) ^ bn(urandom), len(urandom)))
    cfile.close()

    cfileTest = open('file.enc', 'rb').read()

    pass

def decrypt(cfile, kfile, pfile):

    cfile = open(cfile, 'rb').read()
    kfile = open(kfile, 'rb').read()

    pfile = open(pfile, 'wb')
    pfile.write(nb((bn(cfile) ^ bn(kfile)), len(kfile)))
    pfile.close()

    pass

def usage():
    print("Usage:")
    print("encrypt <plaintext file> <output key file> <ciphertext output file>")
    print("decrypt <ciphertext file> <key file> <plaintext output file>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
