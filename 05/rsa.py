#!/usr/bin/env python3

import codecs, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder


# took 4.0 hours


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

#==== ASN1 encoder start ====
def bstrn(bstr):
    i = 0

    for bit in bstr:
        i<<=1
        if(bit =='1'):
            i|=1

    return i


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    ret = 0
    size = sys.getsizeof(value_bytes)

    if(len(value_bytes) >= 128):
        ret = bytes([128 | len(nb(len(value_bytes)))]) + nb(len(value_bytes))
    else:
        ret = bytes([len(value_bytes)])

        if(ret == 0):
            ret = 1
    
    return ret


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'

    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_null():
    return bytes([0x05]) + bytes([0x00])
    


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER

    convertedI = nb(i)
    #print(i,convertedI)

    if(convertedI == b''):
        b = bytes([0x00])
    else:
        if(convertedI[0]>= 128):
            b = bytes([0x00]) + nb(i)
        else:
            b = nb(i)

    return bytes([0x02]) + asn1_len(b) + b

def asn1_bitstring(bitstr):
    if(bitstr == ''):
        return bytes([0x03]) + bytes([0x01]) + bytes([0x0])

    padding = 8 - len(bitstr) % 8
    if(padding == 8):
        padding = 0

    bitstr = bitstr + ('0' * padding)
    i = 0

    bitstrSplitted = [bitstr[i:i+8] for i in range(0, len(bitstr), 8)]
    data = []
    for chunk in bitstrSplitted:
        if(chunk == '00000000'):
            data.append(0)
        else:
            data.append(bstrn(chunk))

    if(padding == 0):
        padding = bytes([0x00])
    else:
        padding = nb(padding)

    res = padding + bytes(data)

    return bytes([0x03]) + asn1_len(res) + res


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    counter = 0
    firstOctet = 40
    biteDataSet = []
    for item in oid:
        if(counter < 1):
            firstOctet = firstOctet * item
            
        elif(counter < 2):
            firstOctet = firstOctet + item
            biteDataSet.append(firstOctet)
        else:
            b = ''
            tempBiteDataSet = []
            prependForFirst = '0' if(item < 255) else '1' 
            lastBinaryUsed = 0
            while item:
                append = '1' if (item % 2) else '0'
                b = b + append

                if(len(b) == 7):
                    if(lastBinaryUsed == 0):
                        prepend = '0'
                        prependForFirst = '1'
                        lastBinaryUsed = 1
                    else:
                        prepend = '1' if(item) else '0' 

                    tempBiteDataSet.insert(0, bstrn(prepend + b[::-1]))

                    b = ''

                item >>= 1
                

            if(b != ''):
                tempBiteDataSet.insert(0, bstrn(prependForFirst + '0' * (7 - len(b[::1])) + b[::-1]))

            biteDataSet = biteDataSet + tempBiteDataSet

        counter = counter + 1


    oid = bytes(biteDataSet)

    return bytes([0x06]) + asn1_len(oid) + oid


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    #constructed
    return bytes([bstrn('110000')]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    #constructed
    return bytes([bstrn('110001')]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    #constructed
    return bytes([bstrn('10011')]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    #constructed
    return bytes([bstrn('10111')]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 + tag]) + asn1_len(der) + der

#==== ASN1 encoder end ====

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == b'--':
        content = content.replace(b"-----BEGIN PUBLIC KEY-----", b"")
        content = content.replace(b"-----END PUBLIC KEY-----", b"")
        content = content.replace(b"-----BEGIN RSA PRIVATE KEY-----", b"")
        content = content.replace(b"-----END RSA PRIVATE KEY-----", b"")
        content = codecs.decode(content, 'base64')
    return content

def get_pubkey(filename):
    # reads public key file and returns (n, e)
    filename = open(filename, 'rb').read()

    # decode the DER to get public key DER structure, which is encoded as BITSTRING
    filename = pem_to_der(filename)

    der = decoder.decode(filename)[0][1]

    # convert BITSTRING to bytestring
    derOclets = 0
    for bit in der:
        derOclets = derOclets << 1 | bit

    # decode the bytestring (which is actually DER) and return (n, e)
    pubkey = decoder.decode(nb(derOclets))[0]

    return int(pubkey[0]), int(pubkey[1])

def get_privkey(filename):
    # reads public key file and returns (n,  d)
    filename = open(filename, 'rb').read()

    filename = pem_to_der(filename)

    privkey = decoder.decode(filename)

    return int(privkey[0][1]), int(privkey[0][3])

def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5

    # calculate number of bytes required to represent the modulus n
    PSsize = len(nb(n))-3 - len(plaintext)

    # plaintext must be at least 11 bytes smaller than the modulus
    if(len(plaintext) + 11 > len(nb(n))):
        print("[-] Error plaintext + 11 is bigger than modulus")
        return False

    random_padding = os.urandom(PSsize)
    random_padding_without_zeros = b''
    for b in random_padding:
        if(b == b'\x00'):
            while b == b'\x00':
                b = os.urandom(1)
        random_padding_without_zeros = random_padding_without_zeros + bytes([b])
    padded_plaintext = bytes([0x00, 0x02]) + random_padding_without_zeros + bytes([0x00]) + plaintext

    # generate padding bytes
    return padded_plaintext

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate number of bytes required to represent the modulus n
    PSsize = len(nb(n)) - 3 - len(plaintext)

    # plaintext must be at least 3 bytes smaller than the modulus
    if(len(plaintext) + 3 > len(nb(n))):
        print("[-] Error plaintext + 3 is bigger than modulus")
        return False

    padded_plaintext = bytes([0x00, 0x01]) + (bytes([0xFF]) * PSsize) + bytes([0x00]) + plaintext


    # generate padding bytes
    return padded_plaintext




def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    end_of_padding = plaintext[2:].find(b'\x00')
    plaintext = plaintext[3+end_of_padding:]

    return plaintext



def encrypt(keyfile, plaintextfile, ciphertextfile):
    plaintextfile = open(plaintextfile, 'rb').read()

    N, e = get_pubkey(keyfile)

    m = pkcsv15pad_encrypt(plaintextfile, N)

    if(m == False):
        return False

    m = bn(m)

    c = pow(m, e, N)
    
    with open(ciphertextfile, 'wb') as f:
        f.write(nb(c))

    pass



def decrypt(keyfile, ciphertextfile, plaintextfile):
    ciphertextfile = open(ciphertextfile, 'rb').read()
    
    priv_key = get_privkey(keyfile)

    c = bn(ciphertextfile)
    N = priv_key[0]
    d = priv_key[1]

    m = nb(pow(c, d, N))

    removed_padding = pkcsv15pad_remove(m)

    f = open(plaintextfile, 'wb')
    f.write(removed_padding)
    f.close()

    pass


def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of file

    messageHash = hashlib.sha256()
    message = open(filename, 'rb')
    chunk = message.read(512)
    while chunk:
        messageHash.update(chunk)
        chunk = message.read(212)
    message.close()

    objectidentifierSha256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]
    der = asn1_sequence(
        asn1_sequence(asn1_objectidentifier(objectidentifierSha256) + 
            asn1_null())+
        asn1_octetstring(messageHash.digest())
    )

    return der

def sign(keyfile, filetosign, signaturefile):

    plaintext = digestinfo_der(filetosign)

    keyfile = get_privkey(keyfile)
    N = keyfile[0]
    d = keyfile[1]

    m = pkcsv15pad_sign(plaintext, N)

    if(m == False):
        pass

    m = bn(m)

    s = pow(m, d, N)

    f = open(signaturefile, 'wb')
    f.write(nb(s, len(nb(N))))
    f.close()

    pass

    # Warning: make sure that signaturefile produced has the same
    # length as the modulus (hint: use parametrized nb()).

def verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification failure"
    signaturefile = open(signaturefile, 'rb').read()

    priv_key = get_pubkey(keyfile)

    s = bn(signaturefile)
    N = priv_key[0]
    e = priv_key[1]

    m = nb(pow(s, e, N))

    removed_padding_asn1 = pkcsv15pad_remove(m)

    if(removed_padding_asn1 == digestinfo_der(filetoverify)):
        print("Verified OK")
    else:
        print("Verification failure")

    pass

def usage():
    print("Usage:")
    print("encrypt <public key file> <plaintext file> <output ciphertext file>")
    print("decrypt <private key file> <ciphertext file> <output plaintext file>")
    print("sign <private key file> <file to sign> <signature output file>")
    print("verify <public key file> <signature file> <file to verify>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'sign':
    sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
