package appcrypto;

import javacard.framework.*;
import javacard.security.*;
import javacardx.crypto.*;

// took 4.0 hours


public class TestApplet extends Applet {
	
	private KeyPair keypair;
	private RSAPublicKey pub;
	private Cipher rsa;
	private RandomData rnd;
	
	public static void install(byte[] ba, short offset, byte len) {
		(new TestApplet()).register();
	}

	private TestApplet() {
		rsa = Cipher.getInstance(Cipher.ALG_RSA_PKCS1, false);
	}
	
	public void process(APDU apdu) {
		byte[] buf = apdu.getBuffer();
		short len, fullMessageLen;
		
		switch (buf[ISO7816.OFFSET_INS]) {
		case (0x02):
			if(keypair == null){
				keypair = new KeyPair(KeyPair.ALG_RSA, KeyBuilder.LENGTH_RSA_2048);
				keypair.genKeyPair();
			}
			return;
		case (0x04):
			if(pub == null){
				pub = (RSAPublicKey) keypair.getPublic();
			}
			len = pub.getExponent(buf, (short)0);
			apdu.setOutgoingAndSend((short)0, len);
			return;
		case (0x06):
			if(pub == null){
				pub = (RSAPublicKey) keypair.getPublic();
			}
			len = pub.getModulus(buf, (short)0);
			apdu.setOutgoingAndSend((short)0, len);
			return;
		case (0x08):
			apdu.setIncomingAndReceive();
			fullMessageLen = (short)(buf[ISO7816.OFFSET_LC] & (short)0xff +(short)2);
			len = (short)(buf[ISO7816.OFFSET_LC] & (short)0xff);
			
			byte[] data = JCSystem.makeTransientByteArray(fullMessageLen, JCSystem.CLEAR_ON_DESELECT);

			Util.arrayCopyNonAtomic(buf, (short)2, data, (short)0, (short)1);
			Util.arrayCopyNonAtomic(buf, (short)3, data, (short)1, (short)1);
			Util.arrayCopyNonAtomic(buf, (short)5, data, (short)2, len);

			rsa.init(keypair.getPrivate(), Cipher.MODE_DECRYPT);
			len = rsa.doFinal(data, (short)0, fullMessageLen, buf, (short)0);

			apdu.setOutgoingAndSend((short)0, len);
			return;
		}
		ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);		

	}
}
