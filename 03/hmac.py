#!/usr/bin/env python3

import codecs, hashlib, sys
from pyasn1.codec.der import decoder
sys.path = sys.path[1:] # don't remove! otherwise the library import below will try to import your hmac.py
import hmac # do not use any other imports/libraries

# took 2.0 hours


#==== ASN1 encoder start ====
def bn(b):    
    # b - bytes to encode as integer
    i = 0
    for x in range(0, len(b)):
        i = i << 8
        i = i | b[x]

    return i

def bstrn(bstr):
    i = 0

    for bit in bstr:
        i<<=1
        if(bit =='1'):
            i|=1

    return i

def nb(i, addLeadingZerosIfNeeded = False):
    # i - integer to encode as bytes
    # length - specifies in how many bytes the number should be encoded
    if(i == 0): return bytes([0x0])
    b = ''
    biteDataSet = []
    while i:
        append = '1' if (i % 2) else '0'
        b = b + append

        if(len(b) == 8):
            biteDataSet.insert(0, bstrn(b[::-1]))
            b = ''

        i >>= 1
        

    if(b != ''):
        biteDataSet.insert(0, bstrn(b[::-1]))

    
    b = bytes(biteDataSet)

    if(addLeadingZerosIfNeeded):
        if(biteDataSet[0] >= 128):
            b = bytes([0x00]) + b

    return b

def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    ret = 0
    size = sys.getsizeof(value_bytes)

    if(len(value_bytes) >= 128):
        ret = bytes([128 | len(nb(len(value_bytes)))]) + nb(len(value_bytes))
    else:
        ret = bytes([len(value_bytes)])

        if(ret == 0):
            ret = 1
    
    return ret


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'

    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_null():
    return bytes([0x05]) + bytes([0x00])
    


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    return bytes([0x02]) + asn1_len(nb(i, True)) + nb(i, True)


def asn1_bitstring(bitstr):
    if(bitstr == ''):
        return bytes([0x03]) + bytes([0x01]) + bytes([0x0])

    padding = 8 - len(bitstr) % 8
    if(padding == 8):
        padding = 0

    bitstr = bitstr + ('0' * padding)
    i = 0

    bitstrSplitted = [bitstr[i:i+8] for i in range(0, len(bitstr), 8)]
    data = []
    for chunk in bitstrSplitted:
        if(chunk == '00000000'):
            data.append(0)
        else:
            data.append(bstrn(chunk))

    res = nb(padding) + bytes(data)

    return bytes([0x03]) + asn1_len(res) + res


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    counter = 0
    firstOctet = 40
    biteDataSet = []
    for item in oid:
        if(counter < 1):
            firstOctet = firstOctet * item
            
        elif(counter < 2):
            firstOctet = firstOctet + item
            biteDataSet.append(firstOctet)
        else:
            b = ''
            tempBiteDataSet = []
            prependForFirst = '0' if(item < 255) else '1' 
            lastBinaryUsed = 0
            while item:
                append = '1' if (item % 2) else '0'
                b = b + append

                if(len(b) == 7):
                    if(lastBinaryUsed == 0):
                        prepend = '0'
                        prependForFirst = '1'
                        lastBinaryUsed = 1
                    else:
                        prepend = '1' if(item) else '0' 

                    tempBiteDataSet.insert(0, bstrn(prepend + b[::-1]))

                    b = ''

                item >>= 1
                

            if(b != ''):
                tempBiteDataSet.insert(0, bstrn(prependForFirst + '0' * (7 - len(b[::1])) + b[::-1]))

            biteDataSet = biteDataSet + tempBiteDataSet

        counter = counter + 1


    oid = bytes(biteDataSet)

    return bytes([0x06]) + asn1_len(oid) + oid


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    #constructed
    return bytes([bstrn('110000')]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    #constructed
    return bytes([bstrn('110001')]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    #constructed
    return bytes([bstrn('10011')]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    #constructed
    return bytes([bstrn('10111')]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 + tag]) + asn1_len(der) + der

#==== ASN1 encoder end ====

def mac(filename):
    key = input("[?] Enter key: ").encode()

    hashType = filename.split("_")[-1]
    objectidentifier = [2, 16, 840, 1, 101, 3, 4, 2, 1]
    hashLibUsed = hashlib.sha256

    if(hashType == 'md5'):
        objectidentifier = [1, 2, 840, 113549, 2, 5]
        hashLibUsed = hashlib.md5

    elif(hashType == 'sha1'):
        objectidentifier = [1, 3, 14, 3, 2, 23]
        hashLibUsed = hashlib.sha1
        
    hmacGenerated = hmac.new(key, None, hashLibUsed)

    message = open(filename, 'rb')
    chunk = message.read(512)
    while chunk:
        hmacGenerated.update(chunk)

        chunk = message.read(212)
    message.close()

    print("[+] Calculated HMAC-" + hashType + ":", hmacGenerated.hexdigest())

    digestInfo = asn1_sequence(asn1_sequence(asn1_objectidentifier(objectidentifier) + asn1_null())+asn1_octetstring(hmacGenerated.digest()))

    hmacFile = open(filename+".hmac", 'wb')
    hmacFile.write(digestInfo)
    hmacFile.close()

    print("[+] Writing HMAC DigestInfo to", filename+".hmac")

def verify(filename):
    print("[+] Reading HMAC DigestInfo from", filename+".hmac")

    der = open(filename+".hmac", 'rb').read()
    digest = nb(bn(decoder.decode(der)[0][1]))
    
    hashType = filename.split("_")[-1]
    hashLibUsed = hashlib.sha256
    if(hashType == 'md5'):
        hashLibUsed = hashlib.md5

    elif(hashType == 'sha1'):
        hashLibUsed = hashlib.sha1

    print("[+] HMAC-" + hashType + ":", nb(bn(decoder.decode(der)[0][1])).hex())

    key = input("[?] Enter key: ").encode()

    hmacGenerated = hmac.new(key, None, hashLibUsed)
    message = open(filename, 'rb')
    chunk = message.read(512)
    while chunk:
        hmacGenerated.update(chunk)
        
        chunk = message.read(212)
    message.close()

    print("[+] Calculated HMAC-" + hashType + ":", hmacGenerated.hexdigest())

    digest_calculated = hmacGenerated.digest()

    if digest_calculated != digest:
        print("[-] Wrong key or message has been manipulated!")
    else:
        print("[+] HMAC verification successful!")



def usage():
    print("Usage:")
    print("-mac <filename>")
    print("-verify <filename>")
    sys.exit(1)

if len(sys.argv) != 3:
    usage()
elif sys.argv[1] == '-mac':
    mac(sys.argv[2])
elif sys.argv[1] == '-verify':
    verify(sys.argv[2])
else:
    usage()
