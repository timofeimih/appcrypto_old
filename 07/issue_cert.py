#!/usr/bin/env python3

import argparse, codecs, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

#took 4.5 hours


# parse arguments
parser = argparse.ArgumentParser(description='issue TLS server certificate based on CSR', add_help=False)
parser.add_argument("CA_cert_file", help="CA certificate (in PEM or DER form)")
parser.add_argument("CA_private_key_file", help="CA private key (in PEM or DER form)")
parser.add_argument("csr_file", help="CSR file (in PEM or DER form)")
parser.add_argument("output_cert_file", help="File to store certificate (in PEM form)")
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i

#==== ASN1 encoder start ====
def bstrn(bstr):
    i = 0

    for bit in bstr:
        i<<=1
        if(bit =='1'):
            i|=1

    return i


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    ret = 0

    if(len(value_bytes) >= 128):
        ret = bytes([128 | len(nb(len(value_bytes)))]) + nb(len(value_bytes))
    else:
        ret = bytes([len(value_bytes)])

        if(ret == 0):
            ret = 1
    
    return ret


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'

    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_null():
    return bytes([0x05]) + bytes([0x00])
    


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER

    convertedI = nb(i)
    #print(i,convertedI)

    if(convertedI == b''):
        b = bytes([0x00])
    else:
        if(convertedI[0]>= 128):
            b = bytes([0x00]) + nb(i)
        else:
            b = nb(i)

    return bytes([0x02]) + asn1_len(b) + b

def asn1_bitstring(bitstr):
    if(bitstr == ''):
        return bytes([0x03]) + bytes([0x01]) + bytes([0x0])

    padding = 8 - len(bitstr) % 8
    if(padding == 8):
        padding = 0

    bitstr = bitstr + ('0' * padding)
    i = 0

    bitstrSplitted = [bitstr[i:i+8] for i in range(0, len(bitstr), 8)]
    data = []
    for chunk in bitstrSplitted:
        data.append(bstrn(chunk))

    if(padding == 0):
        padding = bytes([0x00])
    else:
        padding = nb(padding)

    res = padding + bytes(data)

    return bytes([0x03]) + asn1_len(res) + res

def asn1_bitstring_der(bitstr_der):

    res = bytes([0x00]) + bitstr_der

    return bytes([0x03]) + asn1_len(res) + res

def asn1_utf8(utf8_str):
    res = bytes(utf8_str, 'UTF-8')

    return bytes([0x0c]) + asn1_len(res) + res



def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    counter = 0
    firstOctet = 40
    biteDataSet = []
    for item in oid:
        if(counter < 1):
            firstOctet = firstOctet * item
            
        elif(counter < 2):
            firstOctet = firstOctet + item
            biteDataSet.append(firstOctet)
        else:
            b = ''
            tempBiteDataSet = []
            prependForFirst = '0' if(item < 255) else '1' 
            lastBinaryUsed = 0
            while item:
                append = '1' if (item % 2) else '0'
                b = b + append

                if(len(b) == 7):
                    if(lastBinaryUsed == 0):
                        prepend = '0'
                        prependForFirst = '1'
                        lastBinaryUsed = 1
                    else:
                        prepend = '1' if(item) else '0' 

                    tempBiteDataSet.insert(0, bstrn(prepend + b[::-1]))

                    b = ''

                item >>= 1
                

            if(b != ''):
                tempBiteDataSet.insert(0, bstrn(prependForFirst + '0' * (7 - len(b[::1])) + b[::-1]))

            biteDataSet = biteDataSet + tempBiteDataSet

        counter = counter + 1


    oid = bytes(biteDataSet)

    return bytes([0x06]) + asn1_len(oid) + oid


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    #constructed
    return bytes([bstrn('110000')]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    #constructed
    return bytes([bstrn('110001')]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    #constructed
    return bytes([bstrn('10011')]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    #constructed
    return bytes([bstrn('10111')]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 + tag]) + asn1_len(der) + der

#==== ASN1 encoder end ====

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == b'--':
        content = content.replace(b"-----BEGIN CERTIFICATE REQUEST-----", b"")
        content = content.replace(b"-----END CERTIFICATE REQUEST-----", b"")
        content = content.replace(b"-----BEGIN CERTIFICATE-----", b"")
        content = content.replace(b"-----END CERTIFICATE-----", b"")
        content = content.replace(b"-----BEGIN PUBLIC KEY-----", b"")
        content = content.replace(b"-----END PUBLIC KEY-----", b"")
        content = content.replace(b"-----BEGIN RSA PRIVATE KEY-----", b"")
        content = content.replace(b"-----END RSA PRIVATE KEY-----", b"")
        content = codecs.decode(content, 'base64')
    return content

def der_to_pem(content):
    return b"-----BEGIN CERTIFICATE-----" + b"\n" + codecs.encode(content, 'base64') + b"-----END CERTIFICATE-----"

def get_privkey(filename):
    # reads public key file and returns (n,  d)
    filename = open(filename, 'rb').read()

    filename = pem_to_der(filename)

    privkey = decoder.decode(filename)

    return int(privkey[0][1]), int(privkey[0][3])

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate number of bytes required to represent the modulus n
    PSsize = len(nb(n)) - 3 - len(plaintext)

    # plaintext must be at least 3 bytes smaller than the modulus
    if(len(plaintext) + 3 > len(nb(n))):
        print("[-] Error plaintext + 3 is bigger than modulus")
        return False

    padded_plaintext = bytes([0x00, 0x01]) + (bytes([0xFF]) * PSsize) + bytes([0x00]) + plaintext


    # generate padding bytes
    return padded_plaintext

def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of m

    messageHash = hashlib.sha256(m)

    objectidentifierSha256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]
    der = asn1_sequence(
        asn1_sequence(asn1_objectidentifier(objectidentifierSha256) + 
            asn1_null())+
        asn1_octetstring(messageHash.digest())
    )

    return der

def sign(m, keyfile):
    # signs DigestInfo of message m
    plaintext = digestinfo_der(m)

    keyfile = get_privkey(keyfile)
    N = keyfile[0]
    d = keyfile[1]

    m = pkcsv15pad_sign(plaintext, N)

    if(m == False):
        pass

    m = bn(m)

    s = pow(m, d, N)

    signature = nb(s, len(nb(N)))

    return signature


def get_subject_cn(csr_der):
    # returns CommonName value from CSR's Distinguished Name field

    # looping over Distinguished Name entries until CN found
    for i in decoder.decode(csr_der)[0][0][1]:
        if(str(i[0][0]) == '2.5.4.3'):
            return str(i[0][1])
    pass

def get_subjectPublicKeyInfo(csr_der):
    # returns DER-encoded subjectPublicKeyInfo from CSR
    subjectPKInfo = decoder.decode(csr_der)[0][0][2]

    return encoder.encode(subjectPKInfo)

def get_subjectName(cert_der):
    # returns DER-encoded subject name from CA certificate
    return encoder.encode(decoder.decode(cert_der)[0][0][5])

def issue_certificate(private_key_file, issuer, subject, pubkey):
    # receives CA private key filename, DER-encoded CA Distinguished Name, self-constructed DER-encoded subject's Distinguished Name and DER-encoded subjectPublicKeyInfo
    # returns X.509v3 certificate in PEM format

    m = asn1_sequence(asn1_tag_explicit(asn1_integer(2), 0) + 
        asn1_integer(4138208570) +
        asn1_sequence(asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) + asn1_null()) +
        issuer + 
        asn1_sequence(asn1_utctime(b'210322010100Z')+asn1_utctime(b'220322010100Z')) +
        subject + 
        pubkey + 
        asn1_tag_explicit(
            asn1_sequence(
                asn1_sequence(
                    asn1_objectidentifier([2, 5, 29, 19]) +
                    asn1_boolean(True) +
                    asn1_octetstring(asn1_sequence(asn1_boolean(False)))
                ) +
                asn1_sequence(
                    asn1_objectidentifier([2, 5, 29, 15]) + 
                    asn1_boolean(True) + 
                    asn1_octetstring(asn1_bitstring('1')) 
                ) +
                asn1_sequence(
                    asn1_objectidentifier([2, 5, 29, 37]) + 
                    asn1_octetstring(
                        asn1_sequence(asn1_objectidentifier([1, 3, 6, 1, 5, 5, 7, 3, 1]))
                    )
                    
                )   
            ), 3
        )
    )

    der = asn1_sequence(
        m +
        asn1_sequence( asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) + asn1_null()) + 
        asn1_bitstring_der(sign(m, private_key_file))

    )

    return der_to_pem(der)

# obtain subject's CN from CSR
csr_der = pem_to_der(open(args.csr_file, 'rb').read())
subject_cn_text = get_subject_cn(csr_der)

print("[+] Issuing certificate for \"%s\"" % (subject_cn_text))

# obtain subjectPublicKeyInfo from CSR
pubkey = get_subjectPublicKeyInfo(csr_der)

# construct subject name DN for end-entity's certificate
subject = asn1_sequence(asn1_set(asn1_sequence(asn1_objectidentifier([2, 5, 4, 3]) + asn1_utf8(subject_cn_text))))

# get subject name DN from CA certificate
CAcert = pem_to_der(open(args.CA_cert_file, 'rb').read())
CAsubject = get_subjectName(CAcert)

# issue certificate
cert_pem = issue_certificate(args.CA_private_key_file, CAsubject, subject, pubkey)
open(args.output_cert_file, 'wb').write(cert_pem)
