#!/usr/bin/env python3

# sudo apt install python3-socks
import argparse
import socks
import socket
import sys
import random
# do not use any other imports/libraries

# took 1.5 hours

# parse arguments
parser = argparse.ArgumentParser(description='TorChat client')
parser.add_argument('--myself', required=True, type=str, help='My TorChat ID')
parser.add_argument('--peer', required=True, type=str, help='Peer\'s TorChat ID')
args = parser.parse_args()

# route outgoing connections through Tor
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket

# reads and returns torchat command from the socket
def read_torchat_cmd(incoming_socket):
    # read until newline
    cmd = b''
    while(cmd[-1:] != b"\n"):
    	cmd += incoming_socket.recv(1)
    cmd = cmd[:-1].decode('utf-8')
    # return command
    print("[+] Received:", cmd)

    return cmd

# prints torchat command and sends it
def send_torchat_cmd(outgoing_socket, cmd):
	print("[+] Sending: ", cmd)
	outgoing_socket.send(cmd.encode('utf-8') + b"\n")

# connecting to peer
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("[+] Connecting to peer", args.peer)
s.connect((args.peer + '.onion', 11009))

cookie = str(random.getrandbits(128))

# sending ping
send_torchat_cmd(s, 'ping ' + args.myself + " " + cookie)

# listening for the incoming connection
print("[+] Listening...")
sserv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sserv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sserv.bind(('127.0.0.1', 8888))
sserv.listen(0)

(incoming_socket, address) = sserv.accept()
print("[+] Client %s:%s" % (address[0], address[1]))

incoming_authenticated = False
status_received = False
cookie_peer = ""

# the main loop for processing the received commands
while True:
    cmdr = read_torchat_cmd(incoming_socket)

    cmd = cmdr.split(' ')

    if cmd[0]=='ping':
    	if(cmd[1] == args.peer):
    		cookie_peer = cmd[2]
    elif cmd[0]=='pong':
    	if(cmd[1] == cookie):
    		print("[+] Incoming connection aunthenticated")
    		incoming_authenticated = True

    		send_torchat_cmd(s, 'pong ' + cookie_peer)
    elif cmdr=='status available' and status_received==False:
    	status_received = True
    	send_torchat_cmd(s, 'add_me')
    	send_torchat_cmd(s, 'status available')
    	send_torchat_cmd(s, 'profile_name Alice')
    elif incoming_authenticated == True and cmd[0]=='message':
    	message = input("[?] Enter message: ")
    	send_torchat_cmd(s, 'message ' + message)

