#!/usr/bin/env python3

import datetime, os, sys
from pyasn1.codec.der import decoder

# $ sudo apt-get install python3-crypto
sys.path = sys.path[1:] # removes current directory from aes.py search path
from Crypto.Cipher import AES          # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Util.strxor import strxor  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Util.strxor-module.html#strxor
from hashlib import pbkdf2_hmac
import hashlib, hmac # do not use any other imports/libraries

# took 5.0 hours

#==== ASN1 encoder start ====

def bn(b):    
    # b - bytes to encode as integer
    i = 0
    for x in range(0, len(b)):
        i = i << 8
        i = i | b[x]

    return i



def nb(i, addLeadingZerosIfNeeded = False):
    # i - integer to encode as bytes
    # length - specifies in how many bytes the number should be encoded
    if(i == 0): return bytes([0x0])
    b = ''
    biteDataSet = []
    while i:
        append = '1' if (i % 2) else '0'
        b = b + append

        if(len(b) == 8):
            biteDataSet.insert(0, bstrn(b[::-1]))
            b = ''

        i >>= 1
        

    if(b != ''):
        biteDataSet.insert(0, bstrn(b[::-1]))

    
    b = bytes(biteDataSet)

    if(addLeadingZerosIfNeeded):
        if(biteDataSet[0] >= 128):
            b = bytes([0x00]) + b

    return b

def bstrn(bstr):
    i = 0

    for bit in bstr:
        i<<=1
        if(bit =='1'):
            i|=1

    return i

def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    ret = 0
    size = sys.getsizeof(value_bytes)

    if(len(value_bytes) >= 128):
        ret = bytes([128 | len(nb(len(value_bytes)))]) + nb(len(value_bytes))
    else:
        ret = bytes([len(value_bytes)])

        if(ret == 0):
            ret = 1
    
    return ret


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'

    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_null():
    return bytes([0x05]) + bytes([0x00])
    


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    return bytes([0x02]) + asn1_len(nb(i, True)) + nb(i, True)


def asn1_bitstring(bitstr):
    if(bitstr == ''):
        return bytes([0x03]) + bytes([0x01]) + bytes([0x0])

    padding = 8 - len(bitstr) % 8
    if(padding == 8):
        padding = 0

    bitstr = bitstr + ('0' * padding)
    i = 0

    bitstrSplitted = [bitstr[i:i+8] for i in range(0, len(bitstr), 8)]
    data = []
    for chunk in bitstrSplitted:
        if(chunk == '00000000'):
            data.append(0)
        else:
            data.append(bstrn(chunk))

    res = nb(padding) + bytes(data)

    return bytes([0x03]) + asn1_len(res) + res


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    counter = 0
    firstOctet = 40
    biteDataSet = []
    for item in oid:
        if(counter < 1):
            firstOctet = firstOctet * item
            
        elif(counter < 2):
            firstOctet = firstOctet + item
            biteDataSet.append(firstOctet)
        else:
            b = ''
            tempBiteDataSet = []
            prependForFirst = '0' if(item < 255) else '1' 
            lastBinaryUsed = 0
            while item:
                append = '1' if (item % 2) else '0'
                b = b + append

                if(len(b) == 7):
                    if(lastBinaryUsed == 0):
                        prepend = '0'
                        prependForFirst = '1'
                        lastBinaryUsed = 1
                    else:
                        prepend = '1' if(item) else '0' 

                    tempBiteDataSet.insert(0, bstrn(prepend + b[::-1]))

                    b = ''

                item >>= 1
                

            if(b != ''):
                tempBiteDataSet.insert(0, bstrn(prependForFirst + '0' * (7 - len(b[::1])) + b[::-1]))

            biteDataSet = biteDataSet + tempBiteDataSet

        counter = counter + 1


    oid = bytes(biteDataSet)

    return bytes([0x06]) + asn1_len(oid) + oid


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    #constructed
    return bytes([bstrn('110000')]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    #constructed
    return bytes([bstrn('110001')]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    #constructed
    return bytes([bstrn('10011')]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    #constructed
    return bytes([bstrn('10111')]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 + tag]) + asn1_len(der) + der

#==== ASN1 encoder end ====


# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():

    # measure time for performing 10000 iterations
    start  = datetime.datetime.now()

    password = os.urandom(8)
    salt = os.urandom(8)

    for i in range(0, 10000):
        pbkdf2_hmac('sha1', password, salt, 1000, 48)

    stop = datetime.datetime.now()
    time = (stop-start).total_seconds()

    # extrapolate to 1 second
    iter = int(10000 / time)

    print("[+] Benchmark: %s PBKDF2 iterations in 1 second" % (iter))

    return iter # returns number of iterations that can be performed in 1 second

def encrypt(pfile, cfile):

    iterationCount = 858;

    key = input("[?] Enter key: ").encode()

    salt = os.urandom(8)

    pbkdf2 = pbkdf2_hmac('sha1', key, salt, iterationCount, 48)

    AES128_key = pbkdf2[0:16]
    cipher = AES.new(AES128_key)
    hmac_key = pbkdf2[16:48]

    file = open(pfile, 'rb')
    chunk = file.read(16)
    file_to_chipher = b''

    addedPadding = False
    while chunk:
        if(len(chunk) != 16):
            chunk = chunk + nb((16 - len(chunk))) * (16 - len(chunk))
            addedPadding = True

        file_to_chipher = file_to_chipher + chunk
        chunk = file.read(16)

    if(addedPadding == False):
        file_to_chipher = file_to_chipher + (bytes([0x16] * 16))

    IV = os.urandom(16)
    iv_current = IV
    res = b''
    for chunk in range(0, len(file_to_chipher), 16):
        ciphertext_block = cipher.encrypt(strxor(file_to_chipher[chunk:chunk+16], iv_current))
        res = res + ciphertext_block;
        iv_current = ciphertext_block

    objectidentifierSha256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]
    objectidentifierAES128_cbc = [2, 16, 840, 1, 101, 3, 4, 1, 2]
    hmacGenerated = hmac.new(hmac_key, IV + res, hashlib.sha256)

    digestInfo = asn1_sequence(
        asn1_sequence(asn1_octetstring(salt)+asn1_integer(iterationCount)+asn1_integer(48))+
        asn1_sequence(asn1_objectidentifier(objectidentifierAES128_cbc)+asn1_octetstring(IV))+
        asn1_sequence(asn1_sequence(asn1_objectidentifier(objectidentifierSha256) + asn1_null())+asn1_octetstring(hmacGenerated.digest()))
    )

    cfile = open(cfile, 'wb')
    cfile.write(digestInfo + res)
    cfile.close()

    pass


def decrypt(cfile, pfile):
    cfile = open(cfile, 'rb').read()

    asn1_len = cfile[1]
    starting_bytes = 2
    if(asn1_len > 128):
        margin = asn1_len - 128
        asn1_len = bn(cfile[2: 2+margin])
        starting_bytes = 2 + margin

    ciphertext = cfile[asn1_len + starting_bytes:]


    key = input("[?] Enter key: ").encode()

    der = cfile[:asn1_len + starting_bytes]

    salt = bytes(decoder.decode(der)[0][0][0])
    pbkdf2_iter = int(decoder.decode(der)[0][0][1])
    pbkdf2_key_length = int(decoder.decode(der)[0][0][2])
    hmac_der = bytes(decoder.decode(der)[0][2][1])
    IV = bytes(decoder.decode(der)[0][1][1])

    pbkdf2 = pbkdf2_hmac('sha1', key, salt, pbkdf2_iter, pbkdf2_key_length)

    AES128_key = pbkdf2[0:16]
    cipher = AES.new(AES128_key)
    hmac_key = pbkdf2[16:48]

    hmacGenerated = hmac.new(hmac_key, IV + ciphertext, hashlib.sha256)

    if(not hmac.compare_digest(hmacGenerated.digest(), hmac_der)):
        print("[-] HMAC verification failure: wrong password or modified ciphertext!")
        return


    i = 0
    res = ciphertext
    counter = len(res)
    decrypt_res = b''

    if(counter - 32 >= 0):
        iv_current = res[counter-32: counter-16]
    else: 
        iv_current = IV

    while(counter != 0):
        current = strxor(cipher.decrypt(res[counter-16: counter]), iv_current)
        decrypt_res = current + decrypt_res

        counter = counter - 16

        if(counter - 32 >= 0):
            iv_current = res[counter-32: counter-16]
        else: 
            iv_current = IV
        
        i = i + 1

    res = decrypt_res
    # for g in range(0, len(decrypt_res), 16):
    #     if(decrypt_res[g:g+16] != 16 * bytes([0x16])):
    #         res = res + decrypt_res[g:g+16]
    # if(decrypt_res[-16:] != 16 * bytes([0x16]))

    if(decrypt_res[-16:] != 16 * bytes([0x16])):
        if(decrypt_res[-(decrypt_res[-1]):] == decrypt_res[-1] * bytes([decrypt_res[-1]])):
            res = res[:-(decrypt_res[-1])]
    else:
        res = res[:-16]


    pfile = open(pfile, 'wb')
    pfile.write(res)
    pfile.close()

    pass

def usage():
    print("Usage:")
    print("-encrypt <plaintextfile> <ciphertextfile>")
    print("-decrypt <ciphertextfile> <plaintextfile>")
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()
