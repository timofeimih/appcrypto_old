#!/usr/bin/env python3

# sudo apt install python3-gmpy2
import codecs, hashlib, os, sys # do not use any other imports/libraries
import gmpy2
from secp256r1 import curve
from pyasn1.codec.der import decoder


# took 7.0

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

# --------------- asn1 DER encoder
def bstrn(bstr):
    i = 0

    for bit in bstr:
        i<<=1
        if(bit =='1'):
            i|=1

    return i


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    ret = 0
    size = sys.getsizeof(value_bytes)

    if(len(value_bytes) >= 128):
        ret = bytes([128 | len(nb(len(value_bytes)))]) + nb(len(value_bytes))
    else:
        ret = bytes([len(value_bytes)])

        if(ret == 0):
            ret = 1
    
    return ret


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'

    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_null():
    return bytes([0x05]) + bytes([0x00])
    


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER

    convertedI = nb(i)
    #print(i,convertedI)

    if(convertedI == b''):
        b = bytes([0x00])
    else:
        if(convertedI[0]>= 128):
            b = bytes([0x00]) + nb(i)
        else:
            b = nb(i)

    return bytes([0x02]) + asn1_len(b) + b

def asn1_bitstring(bitstr):
    if(bitstr == ''):
        return bytes([0x03]) + bytes([0x01]) + bytes([0x0])

    padding = 8 - len(bitstr) % 8
    if(padding == 8):
        padding = 0

    bitstr = bitstr + ('0' * padding)
    i = 0

    bitstrSplitted = [bitstr[i:i+8] for i in range(0, len(bitstr), 8)]
    data = []
    for chunk in bitstrSplitted:
        if(chunk == '00000000'):
            data.append(0)
        else:
            data.append(bstrn(chunk))

    if(padding == 0):
        padding = bytes([0x00])
    else:
        padding = nb(padding)

    res = padding + bytes(data)

    return bytes([0x03]) + asn1_len(res) + res


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    counter = 0
    firstOctet = 40
    biteDataSet = []
    for item in oid:
        if(counter < 1):
            firstOctet = firstOctet * item
            
        elif(counter < 2):
            firstOctet = firstOctet + item
            biteDataSet.append(firstOctet)
        else:
            b = ''
            tempBiteDataSet = []
            prependForFirst = '0' if(item < 255) else '1' 
            lastBinaryUsed = 0
            while item:
                append = '1' if (item % 2) else '0'
                b = b + append

                if(len(b) == 7):
                    if(lastBinaryUsed == 0):
                        prepend = '0'
                        prependForFirst = '1'
                        lastBinaryUsed = 1
                    else:
                        prepend = '1' if(item) else '0' 

                    tempBiteDataSet.insert(0, bstrn(prepend + b[::-1]))

                    b = ''

                item >>= 1
                

            if(b != ''):
                tempBiteDataSet.insert(0, bstrn(prependForFirst + '0' * (7 - len(b[::1])) + b[::-1]))

            biteDataSet = biteDataSet + tempBiteDataSet

        counter = counter + 1


    oid = bytes(biteDataSet)

    return bytes([0x06]) + asn1_len(oid) + oid


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    #constructed
    return bytes([bstrn('110000')]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    #constructed
    return bytes([bstrn('110001')]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    #constructed
    return bytes([bstrn('10011')]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    #constructed
    return bytes([bstrn('10111')]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 + tag]) + asn1_len(der) + der

# --------------- asn1 DER encoder end


def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == b'--':
        content = content.replace(b"-----BEGIN PUBLIC KEY-----", b"")
        content = content.replace(b"-----END PUBLIC KEY-----", b"")
        content = content.replace(b"-----BEGIN EC PRIVATE KEY-----", b"")
        content = content.replace(b"-----END EC PRIVATE KEY-----", b"")
        content = codecs.decode(content, 'base64')
    return content

def get_privkey(filename):
    # reads EC private key file and returns the private key integer (d)
    filename = open(filename, 'rb').read()

    priv_key_octet = decoder.decode(pem_to_der(filename))[0][1]

    d = bn(priv_key_octet)

    return d

def get_pubkey(filename):
    # reads EC public key file and returns coordinates (x, y) of the public key point
    filename = open(filename, 'rb').read()

    x_y_bitstring = decoder.decode(pem_to_der(filename))[0][1]


    x_y = 0
    for bit in x_y_bitstring:
        x_y = x_y << 1 | bit

    x_y = nb(x_y)

    if(x_y[0] == 4):
        x_y = x_y[1:]
        x = bn(x_y[:32])
        y = bn(x_y[32:])

    return (x,y)

def ecdsa_sign(keyfile, filetosign, signaturefile):

    # get the private key
    d = get_privkey(keyfile)

    # calculate SHA-384 hash of the file to be signed
    fileHash = hashlib.sha384()
    file = open(filetosign, 'rb')
    chunk = file.read(512)
    while chunk:
        fileHash.update(chunk)
        chunk = file.read(512)
    file.close()

    # truncate the hash value to the curve size
    n = curve.n
    h = fileHash.digest()[0:len(nb(n))]

    # convert hash to integer
    h = bn(h)

    # generate a random nonce k in the range [1, n-1]
    k = bn(os.urandom(len(nb(n))))

    while k < 1 or k > n -1:
        k = bn(os.urandom(len(nb(n))))

    # calculate ECDSA signature components r and s
    R = curve.mul(curve.g, k) 

    r = R[0]

    k_minus_1 = gmpy2.invert(k, n)
    s = (k_minus_1 * (h + (r * d)) % n) % n

    # DER-encode r and s
    der = asn1_sequence(asn1_integer(r) + asn1_integer(s))

    # write DER structure to file
    file = open(signaturefile, 'wb')
    file.write(der)
    file.close()

def ecdsa_verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification failure"
    x, y = get_pubkey(keyfile)

    signaturefile = open(signaturefile, 'rb').read()

    r = int(decoder.decode(signaturefile)[0][0])
    s = int(decoder.decode(signaturefile)[0][1])
    s_minus_1 = gmpy2.invert(s, curve.n)

    G = curve.g

    h = hashlib.sha384()
    file = open(filetoverify, 'rb')
    chunk = file.read(512)
    while chunk:
        h.update(chunk)
        chunk = file.read(512)
    file.close()
    h = h.digest()[0:len(nb(curve.n))]

    d = get_privkey('priv.pem')
    R = curve.add(curve.mul(G, (bn(h) * s_minus_1)), curve.mul([x, y], (r * s_minus_1)))

    if R[0] == r:
        print("Verified OK")
    else:
        print("Verification failure")


def usage():
    print("Usage:")
    print("sign <private key file> <file to sign> <signature output file>")
    print("verify <public key file> <signature file> <file to verify>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'sign':
    ecdsa_sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    ecdsa_verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
