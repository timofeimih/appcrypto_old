#!/usr/bin/env python3

import argparse, codecs, datetime, os, socket, sys, time # do not use any other imports/libraries
from urllib.parse import urlparse

# took 3.0 hours

# parse arguments
parser = argparse.ArgumentParser(description='TLS v1.2 client')
parser.add_argument('url', type=str, help='URL to request')
parser.add_argument('--certificate', type=str, help='File to write PEM-encoded server certificate')
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i

def der_to_pem(content):
    return b"-----BEGIN CERTIFICATE-----" + b"\n" + codecs.encode(content, 'base64') + b"-----END CERTIFICATE-----"

# returns TLS record that contains ClientHello Handshake message
def client_hello():

    print("--> ClientHello()")

    # list of cipher suites the client supports
    csuite = b"\x00\x05" # TLS_RSA_WITH_RC4_128_SHA
    csuite+= b"\x00\x2f" # TLS_RSA_WITH_AES_128_CBC_SHA
    csuite+= b"\x00\x35" # TLS_RSA_WITH_AES_256_CBC_SHA

    # add Handshake message header
    header = bytes([0x16, 0x03, 0x03])
    
    # add record layer header
    hello_body = bytes([0x03, 0x03]) + nb(int(time.time())) + os.urandom(28) + bytes([0x00, 0x00, len(csuite)]) + csuite +  bytes([0x01, 0x00])
    body = bytes([1, 0x00, 0x00, len(hello_body)]) + hello_body
    record = header + bytes([0x00, len(body)]) + body

    return record

# returns TLS record that contains 'Certificate unknown' fatal Alert message
def alert():
    print("--> Alert()")

    # add alert message
    record = bytes([0x02, 46])

    # add record layer header
    record = bytes([0x15, 0x03, 0x03, 0x00, 0x02]) + record

    return record

# parse TLS Handshake messages
def parsehandshake(r):
    global server_hello_done_received

    # read Handshake message type and length from message header
    htype = bytes([r[0]])
    
    if htype == b"\x02":
        print("	<--- ServerHello()")

        gmt = datetime.datetime.fromtimestamp(bn(r[6:10])).strftime('%Y-%m-%d %H:%M;%S')
        server_random = r[10:38]
        sessid = r[39:39+r[38]]
        cipher = r[39+r[38]:41+r[38]]
        compression = bytes([r[41+r[38]]])

        print("	[+] server randomness:", server_random.hex().upper())
        print("	[+] server timestamp:", gmt)
        print("	[+] TLS session ID:", sessid.hex().upper())

        if cipher==b"\x00\x2f":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_128_CBC_SHA")
        elif cipher==b"\x00\x35":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_256_CBC_SHA")
        elif cipher==b"\x00\x05":
            print("	[+] Cipher suite: TLS_RSA_WITH_RC4_128_SHA")
        else:
            print("[-] Unsupported cipher suite selected:", cipher.hex())
            sys.exit(1)

        if compression!=b"\x00":
            print("[-] Wrong compression:", compression.hex())
            sys.exit(1)

    elif htype == b"\x0b":
        print("	<--- Certificate()")

        certlen = bn(r[7:10])
        print("	[+] Server certificate length:", certlen)
        if args.certificate:
            open(args.certificate, 'wb').write(der_to_pem(r[10:10+certlen]))
            print("	[+] Server certificate saved in:", args.certificate)
            
    elif htype == b"\x0e":
        print("	<--- ServerHelloDone()")
        server_hello_done_received = True
        return
    else:
        print("[-] Unknown Handshake type:", htype.hex())
        sys.exit(1)

    # handle the case of several Handshake messages in one record
    leftover = r[4+bn(r[1:4]):]
    if len(leftover):
        parsehandshake(leftover)
    

# parses TLS record
def parserecord(r):
    global s;
    # parse TLS record header and pass the record body to the corresponding parsing method
    ctype = r[0:1]
    c = r[5:]

    # handle known types
    if ctype == b"\x16":
        print("<--- Handshake()")
        parsehandshake(c)
    elif ctype == b"\x15":
        print("<--- Alert()")
        level, desc = c[0], c[1]
        if level == 1:
            print(" [-] warning:", desc)
        elif level == 2:
            print(" [-] fatal:", desc)
            print("[+] Closing TCP connection!")
            s.close()
            sys.exit(1)
        else:
            sys.exit(1)
    else:
        print("[-] Unknown TLS Record type:", ctype.hex())
        sys.exit(1)


# read from the socket full TLS record
def readrecord():
    global s

    record = b""

    # read TLS record header (5 bytes)
    for _ in range(5):
        record += s.recv(1)

    # find data length
    datalen = bn(record[3:5])

    # read TLS record body
    for _ in range(datalen):
        record+= s.recv(1)
        
    return record

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
url = urlparse(args.url)
host = url.netloc.split(':')
if len(host) > 1:
    port = int(host[1])
else:
    port = 443
host = host[0]
path = url.path

s.connect((host, port))
s.send(client_hello())

server_hello_done_received = False
while not server_hello_done_received:
    parserecord(readrecord())

s.send(alert())

print("[+] Closing TCP connection!")
s.close()
