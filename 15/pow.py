#!/usr/bin/env python3

import argparse, hashlib, sys, datetime # do not use any other imports/libraries

# took 2.0 hours


# parse arguments
parser = argparse.ArgumentParser(description='Proof-of-work solver')
parser.add_argument('--difficulty', default=0, type=int, help='Number of leading zero bits')
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

name = b'Timofei'
nonce = 0

counter = 0
start = datetime.datetime.now()

while(counter < args.difficulty):
	nonce += 1
	input = name + nb(nonce, 8)
	res = hashlib.sha256(hashlib.sha256(input).digest()).digest()

	counter = 0
	index = 0
	for i in res:
		if(i == 0):
			counter += 8
			index += 1;

			if(counter >= args.difficulty):
				stop = datetime.datetime.now()
				time = (stop-start).total_seconds()
				break
		else: 
			break

	if(counter+8 >= args.difficulty):
		i = 0
		i <<= 8
		i |= res[index]
		counter += 8 - i.bit_length()

		if(counter >= args.difficulty):
			stop = datetime.datetime.now()
			time = (stop-start).total_seconds()
			break

print('[+] Solved in', time, 'sec (' + str(round(nonce / time / float(1000000), 4)) + ' Mhash/sec)')
print('[+] Input:', input.hex())
print('[+] Solution:', res)
print('[+] Nonce:', nonce)

#output
# [+] Solved in 871.19245 sec (0.2147 Mhash/sec)
# [+] Input: b'Timofei\x00\x00\x00\x00\x0b&7\xab'
# [+] Solution: 000000034dba3258d6c47bdacbf621fbfae6fd3d54d8c3d9ae4a70bcb4b4c291
# [+] Nonce: 187053995


